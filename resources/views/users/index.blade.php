@extends("layouts.app")

@section("content")
    @include("flash::message")
    @include("adminlte-templates::common.errors")

    <div class="box box-primary">
        <section class="content-header">
            <h1 class="title-header">{{ mb_strtoupper(Lang::choice("tables.users", "p"), "UTF-8") }}</h1>
            <a class="table-header-button" href="{{ route('users.create') }}"><i class="fas fa-plus table-header-icon"></i><span class="table-header-text">{{ mb_strtoupper(Lang::get("text.add"), "UTF-8") }}<span></a>
        </section>

        <div class="box-body">
            <div class="row">
                {{-- Deleted Users Field --}}
                <div class="form-group col-md-12">
                    {{ Form::label("deleted_users", Lang::get("text.list_users").":") }}
                    {{ Form::select("deleted_users", [0 => Lang::get("text.not_deleted"), 1 => Lang::get("text.deleted")], null, ["class" => "form-control"]) }}
                </div>
                {{-- Filter Button --}}
                <div class="form-group col-md-12">
                    <a id="filter-btn" href="javascript:void(0);" class="btn btn-primary">{{ Lang::get("text.filter") }}</a>
                    <a id="clear-filter-btn" href="javascript:void(0);" class="btn btn-default">{{ Lang::get('text.clear_filter') }}</a>
                </div>
                <div class="form-group col-md-12">
                    <hr class="no-margin"/>
                </div>
            </div>

            @include("users.table")
        </div>
    </div>
@endsection

@push("js")
    <script type="text/javascript">
        // Select2
        $("#deleted_users").select2({
            placeholder: "Selecionar"
        });

        $(document).ready(function() {
            // Restore deleted_users value
            @if(request()->get("deleted_users"))
                let deletedUsers = @json(request()->get("deleted_users"));
                $("#deleted_users").val(deletedUsers).trigger("change");
            @endif
        });

        // Filter button action
        $(document).on("click", "#filter-btn", function() {
            let deleted_users = $("#deleted_users").val();

            let route = "{{ route('users.index') }}";
            route = route+"?deleted_users="+deleted_users;

            window.location = route;
        });

        // Clear filter button action
        $(document).on("click", "#clear-filter-btn", function() {
            window.location = "{{ route('users.index') }}";
        });
    </script>
@endpush