@push("css")
    @isset($user)
        <style type="text/css">
            .password-field {
                display: none;
            }
        </style>
    @endisset
@endpush

{{-- Name Field --}}
<div class="form-group col-md-6">
    {{ Form::label("name", Lang::get("attributes.name").":") }}
    {{ Form::text("name", null, ["class" => "form-control"]) }}
</div>

{{-- Email Field --}}
<div class="form-group col-md-6">
    {{ Form::label("email", Lang::get("attributes.email").":") }}
    {{ Form::email("email", null, ["class" => "form-control"]) }}
</div>

@isset($user)
    {{-- Keep Password --}}
    <div class="form-group col-md-12">
        <div class="icheck-material-primary">
            {{ Form::checkbox("keep_password", 1, true, ["id" => "keep_password"]) }}
            {{ Form::label("keep_password", Lang::get("attributes.keep_password")) }}
        </div>
    </div>
@endisset

{{-- Password Field --}}
<div class="form-group col-md-6 password-field">
    {{ Form::label("password", Lang::get("attributes.password").":") }}
    {{ Form::password("password", ["class" => "form-control"]) }}
</div>

{{-- Password Confirmation Field --}}
<div class="form-group col-md-6 password-field">
    {{ Form::label("password_confirmation", Lang::get("attributes.password_confirmation").":") }}
    {{ Form::password("password_confirmation", ["class" => "form-control"]) }}
</div>

{{-- Role Name Field --}}
@role(config("enums.roles.ADMIN.name"))
    @php($addressFieldClass = "col-md-6")
    <div class="form-group col-md-6">
        {{ Form::label("role_name", Lang::get("attributes.role_name").":") }}
        {{ Form::select("role_name", ["" => "Selecionar"] + $rolesArray, null, ["class" => "form-control"]) }}
    </div>
@else
    @php($addressFieldClass = "col-md-12")
    {{ Form::hidden("role_name", config("enums.roles.EMPLOYEE.name")) }}
@endrole

{{-- CPF Field --}}
<div class="form-group col-md-6">
    {{ Form::label("cpf", Lang::get("attributes.cpf").":") }}
    {{ Form::text("cpf", null, ["class" => "form-control cpf-mask"]) }}
</div>

{{-- Position Field --}}
<div class="form-group col-md-6">
    {{ Form::label("position", Lang::get("attributes.position").":") }}
    {{ Form::text("position", null, ["class" => "form-control"]) }}
</div>

{{-- Birthday Field --}}
<div class="form-group col-md-6">
    {{ Form::label("birthday", Lang::get("attributes.birthday").":") }}
    {{ Form::text("birthday", $user->readable_birthday?? null, ["class" => "form-control date-mask"]) }}
</div>

{{-- Zipcode Field --}}
<div class="form-group col-md-6">
    {{ Form::label("zipcode", Lang::get("attributes.zipcode").":") }}
    {{ Form::text("zipcode", null, ["class" => "form-control zipcode-mask"]) }}
</div>

{{-- Address Field --}}
<div class="form-group {{ $addressFieldClass }}">
    {{ Form::label("address", Lang::get("attributes.address").":") }}
    {{ Form::text("address", null, ["class" => "form-control", "readonly" => true]) }}
</div>

{{-- Number Field --}}
<div class="form-group col-md-6">
    {{ Form::label("number", Lang::get("attributes.number").":") }}
    {{ Form::text("number", null, ["class" => "form-control", "readonly" => true]) }}
</div>

{{-- Neighborhood Field --}}
<div class="form-group col-md-6">
    {{ Form::label("neighborhood", Lang::get("attributes.neighborhood").":") }}
    {{ Form::text("neighborhood", null, ["class" => "form-control", "readonly" => true]) }}
</div>

{{-- City Field --}}
<div class="form-group col-md-6">
    {{ Form::label("city", Lang::get("attributes.city").":") }}
    {{ Form::text("city", null, ["class" => "form-control", "readonly" => true]) }}
</div>

{{-- State Field --}}
<div class="form-group col-md-6">
    {{ Form::label("state", Lang::get("attributes.state").":") }}
    {{ Form::text("state", null, ["class" => "form-control state-mask", "readonly" => true]) }}
</div>

{{-- Is Active Field --}}
@role(config("enums.roles.ADMIN.name"))
    <div class="form-group col-md-12">
        {{ Form::label("is_active", Lang::get("attributes.is_active").":") }}
        <div class="icheck-material-primary">
            {{ Form::radio("is_active", 1, true, ["id" => "is_active_true"]) }}
            {{ Form::label("is_active_true", Lang::get("text.yes")) }}
        </div>
        <div class="icheck-material-primary">
            {{ Form::radio("is_active", 0, false, ["id" => "is_active_false"]) }}
            {{ Form::label("is_active_false", Lang::get("text.no")) }}
        </div>
    </div>
@else
    {{ Form::hidden("is_active", true) }}
@endrole

{{-- Photo Field --}}
<div class="form-group col-md-12">
    {{ Form::label("photo", Lang::get("attributes.photo").":") }}
    @if(isset($user) && !$user->isPhotoDefault())
        {{-- Preview img --}}
        <div class="restrict-link">
            <a href="{{ $user->photo }}" target="_blank">
                <img class="img-thumbnail" src="{{ $user->photo }}"/>
            </a>
        </div>
        {{-- Delete img --}}
        <div class="icheck-material-primary">
            {{ Form::checkbox("photo", "delete", false, ["id" => "photo"]) }}
            {{ Form::label("photo", Lang::get("attributes.delete_img")) }}
        </div>
    @else
        {{-- Preview img --}}
        <img src=""/>
    @endif
    <div class="custom-file">
        {{ Form::file("photo", ["class" => "form-control custom-file-input"]) }}
        {{ Form::label("photo", Lang::get("text.choose_one"), ["class" => "custom-file-label"]) }}
    </div>
</div>

{{-- Submit Field --}}
<div class="form-group col-md-12 no-margin">
    {{ Form::submit(Lang::get("text.save"), ["class" => "btn btn-primary"]) }}
    @role(config("enums.roles.ADMIN.name"))
        <a href="{{ route('users.index') }}" class="btn btn-default">{{ Lang::get("text.cancel") }}</a>
    @else
        <a href="{{ route('myProfile.show') }}" class="btn btn-default">{{ Lang::get("text.cancel") }}</a>
    @endrole
</div>

@push("js")
    <script type="text/javascript">
        // Select2
        $("#role_name").select2({
            placeholder: "Selecionar"
        });

        // Actions on page load
        $(window).on("pageshow", function() {
            $("#keep_password").trigger("change");
            if ($("#zipcode").val()) {
                toggleAddressFields(false);
            }            
        });

        // Show password fields
        $(document).on("change", "#keep_password", function(){
            if ($(this).is(":checked")) {
                $(".password-field").hide();
            } else {
                $(".password-field").show();
            }
        });

        // Trigger CPF validation
        $(document).on("change", "#cpf", function() {
            let cpf = $(this).val();
            if (cpf) {
                let isValid = validateCPF(cpf);
                if (!isValid) {
                    Swal.fire({
                        title: "{{ Lang::get('text.invalid_value') }}",
                        html: "{{ Lang::get('text.invalid_cpf') }}",
                        icon: "error",
                        showCloseButton: true,
                        showConfirmButton: false
                    });
                    $(this).val("");
                }
            }
        });

        // Validate CPF
        function validateCPF(cpf){
            cpf = cpf.replace(/\D/g, '');
            if(cpf.toString().length != 11 || /^(\d)\1{10}$/.test(cpf)) return false;
            var result = true;
            [9,10].forEach(function(j){
                var soma = 0, r;
                cpf.split(/(?=)/).splice(0,j).forEach(function(e, i){
                    soma += parseInt(e) * ((j+2)-(i+1));
                });
                r = soma % 11;
                r = (r <2)?0:11-r;
                if(r != cpf.substring(j, j+1)) result = false;
            });
            return result;
        }

        // Dynamically fill address
        $(document).on("change", "#zipcode", function() {
            let zipcode = $(this).val();
            if (zipcode && zipcode.indexOf("_")===-1) {
                let route = "{{ route('address.findByZipcode', ':zipcode') }}";
                route = route.replace(":zipcode", zipcode);

                $.ajax({
                    url: route,
                    dataType: "json",
                    type: "GET",
                    beforeSend: function() {
                        $(".alert-ajax").fadeOut(300, "swing");
                        showLoading();
                    },
                    success: function(data) {
                        if (data) {
                            toggleAddressFields(false);

                            $("#address").val(data.logradouro);
                            $("#number").val("");
                            $("#neighborhood").val(data.bairro);
                            $("#city").val(data.localidade);
                            $("#state").val(data.uf);
                        } else {
                            toggleAddressFields(true);

                            $(".alert-ajax").empty();
                            $(".alert-ajax").attr("class", "alert alert-danger alert-ajax");
                            $(".alert-ajax").append("<li>{{ Lang::get('flash.zipcode_not_found') }}</li>");

                            window.scrollTo({top: 0, behavior: "smooth"});
                            $(".alert-ajax").fadeIn(300, "swing");
                        }
                    },
                    error: function(fail) {
                        toggleAddressFields(true);

                        $(".alert-ajax").empty();
                        $(".alert-ajax").attr("class", "alert alert-danger alert-ajax");

                        if (typeof fail.responseJSON.errors !== "undefined") {
                            Object.values(fail.responseJSON.errors).forEach(function(value) {
                                $(".alert-ajax").append("<li>"+value+"</li>");
                            });
                        } else {
                            $(".alert-ajax").append("<li>"+fail.responseJSON.message+"</li>");
                        }

                        window.scrollTo({top: 0, behavior: "smooth"});
                        $(".alert-ajax").fadeIn(300, "swing");
                    },
                    complete: function() {
                        hideLoading();
                    }
                });
            } else {
                toggleAddressFields(true);
            }
        });

        // Toggle address input fields
        function toggleAddressFields(disableFields) {
            if (disableFields) {
                $("#address").val("");
                $("#number").val("");
                $("#neighborhood").val("");
                $("#city").val("");
                $("#state").val("");

                $("#address").attr("readonly", true);
                $("#number").attr("readonly", true);
                $("#neighborhood").attr("readonly", true);
                $("#city").attr("readonly", true);
                $("#state").attr("readonly", true);
            } else {
                $("#address").attr("readonly", false);
                $("#number").attr("readonly", false);
                $("#neighborhood").attr("readonly", false);
                $("#city").attr("readonly", false);
                $("#state").attr("readonly", false);
                if ($("#number").val() == "") {
                    $("#number").focus();
                }
            }
        }
    </script>
@endpush