@extends("layouts.app")

@section("content")
    @include("flash::message")
    @include("adminlte-templates::common.errors")

    <div class="box box-primary">
        <section class="content-header">
            <h1>
                <span class="title-header-small">{{ mb_strtoupper(Lang::get("text.edit"),"UTF-8") }} <i class="fas fa-angle-right"></i></span> 
                @role(config("enums.roles.ADMIN.name"))
                    @php($route = ["users.update", $user->id])
                    <span class="title-header">{{ mb_strtoupper(Lang::choice("tables.users", "s"),"UTF-8") }}</span>
                @else
                    @php($route = ["myProfile.update"])
                    <span class="title-header">{{ mb_strtoupper(Lang::get("text.my_profile"), "UTF-8") }}</span>
                @endif
            </h1>
        </section>

        <div class="box-body">
            {{ Form::model($user, ["route" => $route, "method" => "patch", "files" => true]) }}
                <div class="row">
                    @include("users.fields")
                </div>
            {{ Form::close() }}
        </div>
    </div>
@endsection