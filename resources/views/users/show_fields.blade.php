@isset($user->id)
    {{-- Id Field --}}
    <div class="form-group col-md-12">
        {{ Form::label("id", Lang::get("attributes.id").":") }}
        <p>{{ $user->id }}</p>
    </div>
@endisset

@isset($user->registeredBy)
    {{-- Registered By Field --}}
    <div class="form-group col-md-12">
        {{ Form::label("registered_by", Lang::get("attributes.registered_by").":") }}
        <p>{{ $user->registeredBy->name }}</p>
    </div>
@endisset

@if($user->registeredUsers->isNotEmpty())
    {{-- Registered Users Field --}}
    <div class="form-group col-md-12">
        {{ Form::label("registered_users", Lang::get("attributes.registered_users").":") }}
            <table class="table table-static table-bordered table-striped table-content-size table-scroll-x">
            <thead>
                <tr>
                    <th>{{ Form::label("name", Lang::get("attributes.name"), ["class" => "no-margin"]) }}</th>
                    <th>{{ Form::label("email", Lang::get("attributes.email"), ["class" => "no-margin"]) }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($user->registeredUsers as $registeredUser)
                    <tr>
                        <td>{{ $registeredUser->name?? "-" }}</td>
                        <td>{{ $registeredUser->email?? "-" }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endif

@isset($user->name)
    {{-- Name Field --}}
    <div class="form-group col-md-12">
        {{ Form::label("name", Lang::get("attributes.name").":") }}
        <p>{{ $user->name }}</p>
    </div>
@endisset

@isset($user->email)
    {{-- Email Field --}}
    <div class="form-group col-md-12">
        {{ Form::label("email", Lang::get("attributes.email").":") }}
        <p>{{ $user->email }}</p>
    </div>
@endisset

@isset($user->readable_role_name)
    {{-- Role Field --}}
    <div class="form-group col-md-12">
        {{ Form::label("role_name", Lang::get("attributes.role_name").':') }}
        <p>{{ $user->readable_role_name }}</p>
    </div>
@endisset


@isset($user->cpf)
    {{-- CPF Field --}}
    <div class="form-group col-md-12">
        {{ Form::label("cpf", Lang::get("attributes.cpf").':') }}
        <p>{{ $user->cpf }}</p>
    </div>
@endisset

@isset($user->readable_birthday)
    {{-- Birthday Field --}}
    <div class="form-group col-md-12">
        {{ Form::label("birthday", Lang::get("attributes.birthday").':') }}
        <p>{{ $user->readable_birthday }}</p>
    </div>
@endisset

@isset($user->position)
    {{-- Position Field --}}
    <div class="form-group col-md-12">
        {{ Form::label("position", Lang::get("attributes.position").':') }}
        <p>{{ $user->position }}</p>
    </div>
@endisset

@isset($user->zipcode)
    {{-- Zipcode Field --}}
    <div class="form-group col-md-12">
        {{ Form::label("zipcode", Lang::get("attributes.zipcode").':') }}
        <p>{{ $user->zipcode }}</p>
    </div>
@endisset

@isset($user->address)
    {{-- Address Field --}}
    <div class="form-group col-md-12">
        {{ Form::label("address", Lang::get("attributes.address").':') }}
        <p>{{ $user->address }}</p>
    </div>
@endisset

@isset($user->number)
    {{-- Number Field --}}
    <div class="form-group col-md-12">
        {{ Form::label("number", Lang::get("attributes.number").':') }}
        <p>{{ $user->number }}</p>
    </div>
@endisset

@isset($user->neighborhood)
    {{-- Neighborhood Field --}}
    <div class="form-group col-md-12">
        {{ Form::label("neighborhood", Lang::get("attributes.neighborhood").':') }}
        <p>{{ $user->neighborhood }}</p>
    </div>
@endisset

@isset($user->city)
    {{-- City Field --}}
    <div class="form-group col-md-12">
        {{ Form::label("city", Lang::get("attributes.city").':') }}
        <p>{{ $user->city }}</p>
    </div>
@endisset

@isset($user->state)
    {{-- State Field --}}
    <div class="form-group col-md-12">
        {{ Form::label("state", Lang::get("attributes.state").':') }}
        <p>{{ $user->state }}</p>
    </div>
@endisset




@isset($user->readable_is_active)
    {{-- Is Active Field --}}
    <div class="form-group col-md-12">
        {{ Form::label("is_active", Lang::get("attributes.is_active").":") }}
        <p>{{ $user->readable_is_active }}</p>
    </div>
@endisset

@if(!$user->isPhotoDefault())
    {{-- Photo Field --}}
    <div class="form-group col-md-12">
        {{ Form::label("photo", Lang::get("attributes.photo").":") }}
        <div class="restrict-link no-margin">
            <a href="{{ $user->photo }}" target="_blank">
                <img class="img-thumbnail" src="{{ $user->photo }}"/>
            </a>
        </div>
    </div>
@endif

@isset($user->readable_created_at)
    {{-- Created At Field --}}
    <div class="form-group col-md-12">
        {{ Form::label("created_at", Lang::get("attributes.created_at").":") }}
        <p>{{ $user->readable_created_at }}</p>
    </div>
@endisset

@isset($user->readable_updated_at)
    {{-- Updated At Field --}}
    <div class="form-group col-md-12">
        {{ Form::label("updated_at", Lang::get("attributes.updated_at").":") }}
        <p>{{ $user->readable_updated_at }}</p>
    </div>
@endisset

{{-- Bottom Buttons Field --}}
<div class="form-group col-md-12 no-margin">
    @role(config("enums.roles.ADMIN.name"))
        <a href="{{ route('users.index') }}" class="btn btn-default">{{ Lang::get("text.back") }}</a>
    @else
        <a href="{{ route('myProfile.edit') }}" class="btn btn-primary">{{ Lang::get("text.edit") }}</a>
    @endrole
</div>