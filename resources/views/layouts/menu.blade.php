{{-- Home --}}
<li class="nav-item">
    <a href="{{ route('home') }}" class="nav-link {{ Request::is('*home*') ? 'active' : '' }}">
        <i class="fas fa-home sidebar-icons"></i><p>{{ Lang::choice("text.home", "p") }}</p>
    </a>
</li>

@role(config("enums.roles.ADMIN.name"))
    {{-- User --}}
    <li class="nav-item">
        <a href="{{ route('users.index') }}" class="nav-link {{ Request::is('*users*') ? 'active' : '' }}">
            <i class="fas fa-users sidebar-icons"></i><p>{{ Lang::choice("tables.users", "p") }}</p>
        </a>
    </li>

    {{-- Appointment --}}
    <li class="nav-item">
        <a href="{{ route('appointments.index') }}" class="nav-link {{ Request::is('*appointments*') ? 'active' : '' }}">
            <i class="fas fa-clock sidebar-icons"></i><p>{{ Lang::choice("tables.appointments", "p") }}</p>
        </a>
    </li>
@else
    {{-- Profile --}}
    <li class="nav-item">
        <a href="{{ route('myProfile.show') }}" class="nav-link {{ Request::is('*profile*') ? 'active' : '' }}">
            <i class="fas fa-user sidebar-icons"></i><p>{{ Lang::get("text.my_profile") }}</p>
        </a>
    </li>
@endif

{{-- Logout --}}
<li class="nav-item">
    <a href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
        <i class="fas fa-sign-out-alt sidebar-icons"></i><p>{{ Lang::get("text.logout") }}</p>
    </a>
</li>