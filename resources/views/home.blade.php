@extends('layouts.app')

@section('content')
    <div class="content">
        @include('flash::message')
        @include('adminlte-templates::common.errors')

        @isset($lastAppointment)
            <div class="info-box">
                <span class="info-box-icon bg-primary"><i class="fas fa-clock"></i></span>
                @isset($lastAppointment->end_time)
                    <div class="info-box-content">
                        <span class="info-box-text">{{ Lang::get("text.inactive_appointment") }}</span>
                        <span class="info-box-text"><b>{{ Lang::get("attributes.start_time") }}:</b> {{ $lastAppointment->readable_created_at }}</span>
                        <span class="info-box-text"><b>{{ Lang::get("attributes.end_time") }}:</b> {{ $lastAppointment->readable_end_time }}</span>
                    </div>
                @else
                    {{ Form::model($lastAppointment, ["id" => "appointment-end-form", "route" => ["appointments.end", $lastAppointment->id], "method" => "patch"]) }}
                        <div class="info-box-content">
                            <span class="info-box-text">{{ Lang::get("text.active_appointment") }}</span>
                            <span class="info-box-text"><b>{{ Lang::get("attributes.start_time") }}:</b> {{ $lastAppointment->readable_start_time }}</span>
                            <span class="info-box-text"><b>{{ Lang::get("attributes.end_time") }}:</b> <a href="javascript:void(0);" onclick="document.getElementById('appointment-end-form').submit();">{{ Lang::get("text.end_appointment") }}</a></span>
                        </div>
                    {{ Form::close() }}
                @endisset
            </div>
        @endisset
    </div>
@endsection
