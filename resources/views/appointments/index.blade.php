@extends("layouts.app")

@section("content")
    @include("flash::message")
    @include("adminlte-templates::common.errors")

    <div class="box box-primary">
        <section class="content-header">
            <h1 class="title-header">{{ mb_strtoupper(Lang::choice("tables.appointments", "p"), "UTF-8") }}</h1>
        </section>

        <div class="box-body">
            <div class="row">
                {{-- Start Time Field --}}
                <div class="form-group col-md-6">
                    {{ Form::label("start_time", Lang::get("attributes.start_time").":") }}
                    {{ Form::text("start_time", null, ["class" => "form-control date-range-mask"]) }}
                </div>
                {{-- End Time Field --}}
                <div class="form-group col-md-6">
                    {{ Form::label("end_time", Lang::get("attributes.end_time").":") }}
                    {{ Form::text("end_time", null, ["class" => "form-control date-range-mask"]) }}
                </div>
                {{-- Filter Button --}}
                <div class="form-group col-md-12">
                    <a id="filter-btn" href="javascript:void(0);" class="btn btn-primary">{{ Lang::get("text.filter") }}</a>
                    <a id="clear-filter-btn" href="javascript:void(0);" class="btn btn-default">{{ Lang::get('text.clear_filter') }}</a>
                </div>
                <div class="form-group col-md-12">
                    <hr class="no-margin"/>
                </div>
            </div>

            @include("appointments.table")
        </div>
    </div>
@endsection

@push("js")
    <script type="text/javascript">
        $(document).ready(function() {
            // Date range picker locale
            moment.locale("pt_br");

            // Restore start_time value
            @if(request()->get("start_time"))
                let startTime = @json(explode(" - ", request()->get("start_time")));
                $("#start_time").val(startTime[0] + " - " + startTime[1]);
            @endif

            // Restore end_time value
            @if(request()->get("end_time"))
                let endTime = @json(explode(" - ", request()->get("end_time")));
                $("#end_time").val(endTime[0] + " - " + endTime[1]);
            @endif

            // Aplly date range picker on start_time
            $("#start_time").daterangepicker({
                locale: {
                    "applyLabel": "Aplicar",
                    "cancelLabel": "Cancelar",
                },
                autoUpdateInput: false,
                autoApply: true,
            });
            $("#start_time").on("apply.daterangepicker", function(ev, picker) {
                $(this).val(picker.startDate.format("DD/MM/YYYY") + " - " + picker.endDate.format("DD/MM/YYYY"));
            });
            $("#start_time").on("cancel.daterangepicker", function(ev, picker) {
                $(this).val("");
            });

            // Aplly date range picker on end_time
            $("#end_time").daterangepicker({
                locale: {
                    "applyLabel": "Aplicar",
                    "cancelLabel": "Cancelar",
                },
                autoUpdateInput: false,
                autoApply: true,
            });
            $("#end_time").on("apply.daterangepicker", function(ev, picker) {
                $(this).val(picker.startDate.format("DD/MM/YYYY") + " - " + picker.endDate.format("DD/MM/YYYY"));
            });
            $("#end_time").on("cancel.daterangepicker", function(ev, picker) {
                $(this).val("");
            });
        });

        // Filter button action
        $(document).on("click", "#filter-btn", function() {
            let startTime = $("#start_time").val();
            let endTime = $("#end_time").val();

            let route = "{{ route('appointments.index') }}";
            route = route+"?start_time="+startTime+"&end_time="+endTime;

            window.location = route;
        });

        // Clear filter button action
        $(document).on("click", "#clear-filter-btn", function() {
            window.location = "{{ route('appointments.index') }}";
        });

        // Date range mask
        $(document).on("focus", ".date-range-mask", function(){
            $(this).inputmask("text", {
                "mask": ["99/99/9999 - 99/99/9999"],
                "clearMaskOnLostFocus": true,
                "showMaskOnHover": false,
                "showMaskOnFocus": false,
                "rightAlign": false,
                "removeMaskOnSubmit": false,
                "autoUnmask": false,
                "onincomplete": function() {
                    if (this.value) {
                        this.value = "";
                        Swal.fire({
                            title: "Valor inválido!",
                            html: "Informe um intervalo no formato<br/><u>dd/mm/aaaa - dd/mm/aaaa</u>.",
                            icon: "error",
                            showCloseButton: true,
                            showConfirmButton: false
                        });
                    }
                },
                "oncomplete": function() {
                    dateRange = this.value.split(" - ");
                    startRange = moment(dateRange[0]);
                    endRange = moment(dateRange[1]);
                    if (!startRange.isValid() || !endRange.isValid()) {
                        this.value = "";
                        $(".daterangepicker").hide();
                        Swal.fire({
                            title: "Valor inválido!",
                            html: "O intervalo informado não é válido, tente novamente.",
                            icon: "error",
                            showCloseButton: true,
                            showConfirmButton: false
                        });
                    }
                },
            });
        });
    </script>
@endpush