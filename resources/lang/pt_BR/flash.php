<?php

return [

    // Models
    "updated"                       => "[m] atualizado com sucesso!                                         |[f] atualizada com sucesso!",
    "updated_plural"                => "[m] atualizados com sucesso!                                        |[f] atualizadas com sucesso!",
    "not_updated"                   => "[m] não atualizado.                                                 |[f] não atualizada.",

    "saved"                         => "[m] criado com sucesso!                                             |[f] criada com sucesso!",
    "saved_plural"                  => "[m] criados com sucesso!                                            |[f] criadas com sucesso!",

    "deleted"                       => "[m] restaurado com sucesso!                                         |[f] deletada com sucesso!",
    "deleted_plural"                => "[m] deletados com sucesso!                                          |[f] deletadas com sucesso!",
    "not_deleted"                   => "[m] não pode ser deletado pois há outros registros atrelados a ele. |[f] não pode ser deletada pois há outros registros atrelados a ela.",

    "restored"                      => "[m] restaurado com sucesso!                                         |[f] restaurada com sucesso!",

    "added"                         => "[m] adicionado com sucesso!                                         |[f] adicionada com sucesso!",
    "added_plural"                  => "[m] adicionados com sucesso!                                        |[f] adicionadas com sucesso!",

    "removed"                       => "[m] removido com sucesso!                                           |[f] removida com sucesso!",
    "removed_plural"                => "[m] removidos com sucesso!                                          |[f] removidas com sucesso!",

    "not_found"                     => "[m] não encontrado.                                                 |[f] não encontrada.",
    "not_found_plural"              => "[m] não encontrados.                                                |[f] não encontradas.",

    "finished"                      => "[m] encerrado com sucesso!                                          |[f] encerrada com sucesso!",
    "completed"                     => "[m] completado com sucesso!                                         |[f] completada com sucesso!",
    "inactivated"                   => "[m] inativado com sucesso!                                          |[f] inativada com sucesso!",
    "activated"                     => "[m] ativado com sucesso!                                            |[f] ativada com sucesso!",
    "imported"                      => "[m] importados com sucesso!                                         |[f] importadas com sucesso!",

    // Errors
    "403"                           => "Você não tem permissão para acessar a página requisitada.",
    "404"                           => "A página requisitada não existe.",
    "invalid_role"                  => "O perfil selecionado não é válido.",
    "invalid_login"                 => "Você não tem permissão para acessar o sistema.",
    "self_delete"                   => "Você não pode deletar o seu próprio usuário!",
    "self_inactivate"               => "Você não pode inativar o seu próprio usuário!",
    "self_role_change"              => "Você não pode alterar o seu próprio perfil!",
    "inactive_user"                 => "Seu usuário foi desativado.",
    "registration_unexpected_error" => "Não foi possível concluir seu cadastro, um erro inesperado aconteceu.",
    "zipcode_not_found"             => "O CEP informado não foi encontrado.",
    "cannot_edit_admin"             => "Você não pode editar outros usuários administradores.",
    "cannot_delete_admin"           => "Você não pode deletar outros usuários administradores.",
];
