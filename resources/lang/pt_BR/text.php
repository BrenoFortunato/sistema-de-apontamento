<?php

return [

    // Actions
    "save"                  => "Salvar",
    "cancel"                => "Cancelar",
    "back"                  => "Voltar",
    "confirm"               => "Confirmar",
    "add"                   => "Adicionar",
    "remove"                => "Remover",
    "restore"               => "Restaurar",
    "edit"                  => "Editar",
    "details"               => "Detalhes",
    "filter"                => "Filtrar",
    "check"                 => "Dispensar",
    "import"                => "Importar",
    "export"                => "Exportar",
    "reports"               => "Relatórios",
    "choose_one"            => "Escolha um arquivo...",
    "choose_many"           => "Escolha vários arquivos...",
    "filter"                => "Filtrar",
    "clear_filter"          => "Limpar Filtros",
    "end_appointment"       => "clique aqui para encerrar",
    
    // Messages
    "yes"                   => "Sim",
    "no"                    => "Não",
    "member_since"          => "Membro desde",
    "confirmation_title"    => "Você tem certeza?",
    "confirmation_text"     => "Não se preocupe, caso deseje você poderá recuperar o registro deletado posteriormente.",
    "invalid_value"         => "Valor inválido!",
    "invalid_cpf"           => "O CPF informado não é válido.",
    "active_appointment"    => "Apontamento ativo",
    "inactive_appointment"  => "Apontamento encerrado",
    "list_users"            => "Listar Usuários",
    "not_deleted"           => "Não deletados",
    "deleted"               => "Deletados",

    // Menu
    "home"                  => "Início",
    "profile"               => "Perfil",
    "my_profile"            => "Meu Perfil",
    "logout"                => "Sair",

];
