<?php

return [

    // User
    "id"                       => "Id",
    "registered_by"            => "Registrado por",
    "registered_users"         => "Usuários Registrados",
    "name"                     => "Nome",
    "email"                    => "E-mail",
    "password"                 => "Senha",
    "password_confirmation"    => "Confirmar Senha",
    "keep_password"            => "Manter Senha",
    "role_name"                => "Perfil",
    "cpf"                      => "CPF",
    "position"                 => "Cargo",
    "birthday"                 => "Aniversário",
    "age"                      => "Idade",
    "zipcode"                  => "CEP",
    "address"                  => "Endereço",
    "number"                   => "Número",
    "neighborhood"             => "Bairro",
    "city"                     => "Cidade",
    "state"                    => "Estado",
    "full_address"             => "Endereço Completo",
    "is_active"                => "Ativo",
    "photo"                    => "Foto",
    "delete_img"               => "Deletar Foto",
    "email_verified_at"        => "E-mail Verificado em",
    "created_at"               => "Criado em",
    "updated_at"               => "Atualizado em",

    // Appointment
    "start_time"               => "Início",
    "end_time"                 => "Término",
    "user_id"                  => "Usuário",
    "duration"                 => "Duração",
    
];
