<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth
Auth::routes(["register" => false]);

// Landing Page
Route::get("/", function () {
    return redirect(route("home"));
});

Route::group(["middleware" => ["auth", "active"]], function () {
    // Home
    Route::get("/home",                                            ["as" => "home",        "uses" => "HomeController@index"]);

    // Address
    Route::group(["prefix" => "address"], function () {
        Route:: get("/find-by-zipcode/{zipcode}",                  ["as" => "address.findByZipcode", "uses" => "AddressController@findByZipcode"]);
    });
    
    // Admin Exclusive
    Route::group(["middleware" => ["role:admin"]], function () {
        // User
        Route::group(["prefix" => "users"], function () {
            Route::get("/",                                            ["as" => "users.index",   "uses" => "UserController@index"]);
            Route::get("/create",                                      ["as" => "users.create",  "uses" => "UserController@create"]);
            Route::post("/",                                           ["as" => "users.store",   "uses" => "UserController@store"]);
            Route::get("/{user_id}",                                   ["as" => "users.show",    "uses" => "UserController@show"]);
            Route::match(["put", "patch"], "/{user_id}",               ["as" => "users.update",  "uses" => "UserController@update"]);
            Route::delete("/{user_id}",                                ["as" => "users.destroy", "uses" => "UserController@destroy"]);
            Route::get("/{user_id}/edit",                              ["as" => "users.edit",    "uses" => "UserController@edit"]);
            Route::match(["put", "patch"], "/{user_id}/restore",       ["as" => "users.restore", "uses" => "UserController@restore"]);
        });

        // Appointment
        Route::group(["prefix" => "appointments"], function () {
            Route::get("/",                                            ["as" => "appointments.index",   "uses" => "AppointmentController@index"]);
        });
    });

    // Employee Exclusive
    Route::group(["middleware" => ["role:employee"]], function () {
        // User
        Route::group(["prefix" => "my-profile"], function () {
            Route::get("/",                                   ["as" => "myProfile.show",    "uses" => "UserController@show"]);
            Route::match(["put", "patch"], "/",               ["as" => "myProfile.update",  "uses" => "UserController@update"]);
            Route::get("/edit",                               ["as" => "myProfile.edit",    "uses" => "UserController@edit"]);
        });

        // Appointment
        Route::group(["prefix" => "appointments"], function () {
            Route::match(["put", "patch"], "/{appointment_id}/end",        ["as" => "appointments.end",  "uses" => "AppointmentController@end"]);
        });
    });
});