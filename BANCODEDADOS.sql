-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: appointment_system
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `appointments`
--

DROP TABLE IF EXISTS `appointments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `appointments` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `appointments_user_id_foreign` (`user_id`),
  CONSTRAINT `appointments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appointments`
--

LOCK TABLES `appointments` WRITE;
/*!40000 ALTER TABLE `appointments` DISABLE KEYS */;
INSERT INTO `appointments` VALUES (1,2,'2021-04-19 17:50:02','2021-04-20 08:45:56','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(2,2,'2021-05-01 02:03:39','2021-05-01 02:39:42','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(3,2,'2021-05-06 10:45:15','2021-05-07 03:02:01','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(4,2,'2021-05-14 23:01:24','2021-05-15 14:20:13','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(5,2,'2021-05-09 01:46:03','2021-05-09 13:40:05','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(6,2,'2021-05-18 01:09:46','2021-05-18 03:50:24','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(7,2,'2021-05-03 17:43:32','2021-05-03 17:49:06','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(8,2,'2021-05-13 20:15:58','2021-05-14 01:53:32','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(9,2,'2021-04-20 06:33:58','2021-04-20 16:48:46','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(10,2,'2021-04-25 10:55:55','2021-04-26 01:04:17','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(11,3,'2021-05-06 20:23:05','2021-05-06 22:24:10','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(12,3,'2021-05-07 15:34:27','2021-05-07 20:39:03','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(13,3,'2021-05-09 18:28:48','2021-05-09 19:08:13','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(14,3,'2021-04-21 01:36:25','2021-04-22 00:07:43','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(15,3,'2021-05-06 03:10:32','2021-05-06 06:25:42','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(16,3,'2021-05-17 08:53:30','2021-05-17 22:53:54','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(17,3,'2021-05-13 08:24:53','2021-05-13 10:20:11','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(18,3,'2021-04-25 03:59:55','2021-04-25 11:39:46','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(19,3,'2021-05-09 21:22:27','2021-05-10 15:18:02','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(20,3,'2021-04-23 06:05:28','2021-04-23 07:21:53','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(21,4,'2021-04-28 03:50:45','2021-04-29 03:11:01','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(22,4,'2021-04-23 23:25:44','2021-04-24 16:58:39','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(23,4,'2021-05-11 16:35:48','2021-05-11 22:34:24','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(24,4,'2021-04-29 08:08:46','2021-04-30 03:35:54','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(25,4,'2021-05-01 19:19:12','2021-05-02 01:49:24','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(26,4,'2021-05-11 07:11:24','2021-05-12 02:18:14','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(27,4,'2021-05-16 04:47:52','2021-05-16 16:44:02','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(28,4,'2021-05-02 09:21:34','2021-05-02 09:32:53','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(29,4,'2021-04-22 08:24:15','2021-04-23 02:31:17','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(30,4,'2021-04-30 09:28:35','2021-04-30 16:18:44','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(31,5,'2021-05-12 16:51:28','2021-05-13 03:20:45','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(32,5,'2021-05-18 03:07:13','2021-05-18 07:34:57','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(33,5,'2021-05-12 23:55:16','2021-05-13 12:24:41','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(34,5,'2021-05-10 12:01:40','2021-05-10 16:39:58','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(35,5,'2021-04-23 18:54:29','2021-04-24 04:13:26','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(36,5,'2021-05-05 21:07:55','2021-05-05 23:47:09','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(37,5,'2021-04-25 14:21:49','2021-04-26 11:22:42','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(38,5,'2021-04-23 16:32:44','2021-04-24 04:52:38','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(39,5,'2021-05-12 15:00:13','2021-05-13 04:54:45','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(40,5,'2021-05-13 05:17:00','2021-05-13 06:09:52','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(41,6,'2021-04-27 02:17:37','2021-04-27 06:59:54','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(42,6,'2021-05-12 15:26:40','2021-05-13 01:58:54','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(43,6,'2021-05-04 21:25:55','2021-05-05 01:19:19','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(44,6,'2021-04-24 04:25:38','2021-04-25 02:56:48','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(45,6,'2021-04-30 10:34:40','2021-05-01 06:42:46','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(46,6,'2021-05-10 10:47:37','2021-05-10 19:08:13','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(47,6,'2021-05-07 13:32:25','2021-05-08 04:02:36','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(48,6,'2021-04-30 22:29:05','2021-05-01 20:12:51','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(49,6,'2021-05-18 12:43:51','2021-05-19 01:30:48','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(50,6,'2021-05-07 13:07:48','2021-05-08 10:59:17','2021-05-20 00:51:51','2021-05-20 00:51:51',NULL),(51,8,'2021-04-20 09:15:46','2021-04-20 16:24:29','2021-05-20 00:51:52','2021-05-20 00:51:52',NULL),(52,8,'2021-05-06 10:46:05','2021-05-07 03:36:10','2021-05-20 00:51:52','2021-05-20 00:51:52',NULL),(53,8,'2021-04-26 13:15:12','2021-04-26 19:04:25','2021-05-20 00:51:52','2021-05-20 00:51:52',NULL),(54,8,'2021-05-14 00:48:02','2021-05-14 13:22:45','2021-05-20 00:51:52','2021-05-20 00:51:52',NULL),(55,8,'2021-04-22 18:14:10','2021-04-23 01:22:37','2021-05-20 00:51:52','2021-05-20 00:51:52',NULL),(56,8,'2021-05-17 13:20:49','2021-05-18 09:01:49','2021-05-20 00:51:52','2021-05-20 00:51:52',NULL),(57,8,'2021-05-14 20:54:49','2021-05-15 08:23:13','2021-05-20 00:51:52','2021-05-20 00:51:52',NULL),(58,8,'2021-05-05 07:33:53','2021-05-06 04:42:58','2021-05-20 00:51:52','2021-05-20 00:51:52',NULL),(59,8,'2021-04-23 10:34:31','2021-04-24 06:03:46','2021-05-20 00:51:52','2021-05-20 00:51:52',NULL),(60,8,'2021-04-19 14:03:13','2021-04-20 13:56:04','2021-05-20 00:51:52','2021-05-20 00:51:52',NULL),(61,9,'2021-05-16 04:53:41','2021-05-17 01:49:54','2021-05-20 00:51:52','2021-05-20 00:51:52',NULL),(62,9,'2021-05-08 05:34:35','2021-05-08 22:59:35','2021-05-20 00:51:52','2021-05-20 00:51:52',NULL),(63,9,'2021-04-25 03:39:16','2021-04-25 17:04:47','2021-05-20 00:51:52','2021-05-20 00:51:52',NULL),(64,9,'2021-05-03 15:11:33','2021-05-04 01:54:59','2021-05-20 00:51:52','2021-05-20 00:51:52',NULL),(65,9,'2021-05-05 16:00:53','2021-05-05 23:25:13','2021-05-20 00:51:52','2021-05-20 00:51:52',NULL),(66,9,'2021-05-07 23:39:30','2021-05-08 15:02:21','2021-05-20 00:51:52','2021-05-20 00:51:52',NULL),(67,9,'2021-05-16 13:08:44','2021-05-16 18:33:50','2021-05-20 00:51:52','2021-05-20 00:51:52',NULL),(68,9,'2021-04-19 19:00:53','2021-04-20 06:21:57','2021-05-20 00:51:52','2021-05-20 00:51:52',NULL),(69,9,'2021-05-17 02:55:39','2021-05-17 07:17:37','2021-05-20 00:51:52','2021-05-20 00:51:52',NULL),(70,9,'2021-05-15 08:11:41','2021-05-15 08:27:41','2021-05-20 00:51:52','2021-05-20 00:51:52',NULL),(71,10,'2021-05-12 22:51:41','2021-05-13 11:41:02','2021-05-20 00:51:52','2021-05-20 00:51:52',NULL),(72,10,'2021-05-12 19:14:34','2021-05-13 11:10:30','2021-05-20 00:51:52','2021-05-20 00:51:52',NULL),(73,10,'2021-05-03 19:07:31','2021-05-04 07:26:53','2021-05-20 00:51:52','2021-05-20 00:51:52',NULL),(74,10,'2021-05-12 10:20:15','2021-05-13 03:02:08','2021-05-20 00:51:52','2021-05-20 00:51:52',NULL),(75,10,'2021-05-16 03:57:02','2021-05-17 01:07:35','2021-05-20 00:51:52','2021-05-20 00:51:52',NULL),(76,10,'2021-05-11 09:36:26','2021-05-12 02:48:32','2021-05-20 00:51:52','2021-05-20 00:51:52',NULL),(77,10,'2021-04-24 08:31:50','2021-04-24 08:55:50','2021-05-20 00:51:52','2021-05-20 00:51:52',NULL),(78,10,'2021-04-30 10:51:33','2021-05-01 10:51:27','2021-05-20 00:51:52','2021-05-20 00:51:52',NULL),(79,10,'2021-04-21 11:20:31','2021-04-22 10:03:42','2021-05-20 00:51:52','2021-05-20 00:51:52',NULL),(80,10,'2021-05-01 17:10:59','2021-05-02 12:57:49','2021-05-20 00:51:52','2021-05-20 00:51:52',NULL),(81,11,'2021-04-30 18:33:11','2021-05-01 05:26:29','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(82,11,'2021-04-20 22:59:05','2021-04-21 00:48:08','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(83,11,'2021-05-01 07:46:06','2021-05-02 07:27:17','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(84,11,'2021-05-11 09:36:54','2021-05-12 05:04:44','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(85,11,'2021-04-23 13:33:46','2021-04-23 15:31:18','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(86,11,'2021-04-20 14:32:02','2021-04-21 07:41:54','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(87,11,'2021-04-27 10:10:39','2021-04-28 06:26:25','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(88,11,'2021-05-11 23:31:00','2021-05-12 12:33:53','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(89,11,'2021-04-19 16:15:57','2021-04-20 00:15:31','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(90,11,'2021-04-23 17:57:12','2021-04-24 01:34:18','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(91,12,'2021-04-26 18:40:34','2021-04-27 08:31:38','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(92,12,'2021-05-07 11:06:16','2021-05-07 16:11:58','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(93,12,'2021-05-04 23:04:07','2021-05-05 09:56:40','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(94,12,'2021-04-21 02:38:19','2021-04-21 16:03:39','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(95,12,'2021-04-21 20:05:59','2021-04-22 10:33:05','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(96,12,'2021-05-10 17:36:12','2021-05-11 11:22:40','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(97,12,'2021-04-26 18:42:08','2021-04-27 08:27:16','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(98,12,'2021-05-15 15:52:01','2021-05-16 10:43:35','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(99,12,'2021-05-08 12:58:34','2021-05-09 06:37:29','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(100,12,'2021-05-16 01:41:04','2021-05-16 17:19:31','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(101,14,'2021-04-26 22:25:26','2021-04-27 18:06:19','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(102,14,'2021-04-25 02:40:23','2021-04-25 16:51:38','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(103,14,'2021-04-30 06:09:59','2021-04-30 16:53:28','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(104,14,'2021-04-23 03:27:42','2021-04-23 08:28:43','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(105,14,'2021-04-19 22:00:24','2021-04-20 16:20:48','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(106,14,'2021-05-10 03:51:06','2021-05-10 09:43:35','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(107,14,'2021-04-22 14:37:08','2021-04-22 23:00:46','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(108,14,'2021-04-29 07:32:38','2021-04-30 04:24:36','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(109,14,'2021-04-21 03:03:02','2021-04-22 00:21:23','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(110,14,'2021-05-06 18:38:55','2021-05-07 11:49:15','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(111,15,'2021-04-28 06:39:19','2021-04-28 20:23:35','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(112,15,'2021-05-17 02:15:18','2021-05-17 16:34:57','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(113,15,'2021-04-19 20:16:31','2021-04-20 04:52:26','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(114,15,'2021-04-25 22:20:01','2021-04-26 11:40:24','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(115,15,'2021-04-19 03:01:56','2021-04-19 23:08:53','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(116,15,'2021-05-03 11:43:16','2021-05-03 23:26:36','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(117,15,'2021-05-07 17:01:53','2021-05-08 08:14:55','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(118,15,'2021-04-29 07:53:26','2021-04-30 07:22:10','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(119,15,'2021-05-18 03:18:10','2021-05-18 05:53:00','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(120,15,'2021-05-07 01:28:57','2021-05-07 23:06:56','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(121,16,'2021-05-18 05:44:57','2021-05-18 09:23:07','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(122,16,'2021-04-25 02:51:14','2021-04-25 06:33:22','2021-05-20 00:51:53','2021-05-20 00:51:53',NULL),(123,16,'2021-04-21 16:50:04','2021-04-21 21:19:09','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(124,16,'2021-04-25 01:56:11','2021-04-25 20:50:54','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(125,16,'2021-05-11 23:55:41','2021-05-12 12:48:03','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(126,16,'2021-04-22 23:02:43','2021-04-23 08:00:09','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(127,16,'2021-05-02 09:43:45','2021-05-02 16:50:57','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(128,16,'2021-04-26 23:06:43','2021-04-26 23:19:59','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(129,16,'2021-05-05 01:58:48','2021-05-05 16:52:49','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(130,16,'2021-05-14 02:32:24','2021-05-14 19:07:11','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(131,17,'2021-05-08 00:07:44','2021-05-08 04:24:43','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(132,17,'2021-05-05 12:57:15','2021-05-05 16:35:35','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(133,17,'2021-05-01 07:37:43','2021-05-02 01:03:06','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(134,17,'2021-05-08 13:20:57','2021-05-08 15:21:17','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(135,17,'2021-05-17 12:46:22','2021-05-17 16:21:59','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(136,17,'2021-05-15 13:19:38','2021-05-15 23:33:13','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(137,17,'2021-04-23 04:33:26','2021-04-23 06:10:01','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(138,17,'2021-04-22 20:02:26','2021-04-23 10:43:25','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(139,17,'2021-05-06 07:03:06','2021-05-06 08:53:58','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(140,17,'2021-05-07 15:17:35','2021-05-08 01:33:21','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(141,18,'2021-04-22 04:59:50','2021-04-23 02:21:14','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(142,18,'2021-04-28 12:44:26','2021-04-29 04:56:40','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(143,18,'2021-04-29 06:39:08','2021-04-29 20:45:20','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(144,18,'2021-05-11 07:56:15','2021-05-11 23:32:36','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(145,18,'2021-05-14 22:25:34','2021-05-15 07:59:47','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(146,18,'2021-04-22 20:50:13','2021-04-22 21:40:32','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(147,18,'2021-05-12 05:22:37','2021-05-12 09:53:01','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(148,18,'2021-04-23 11:51:41','2021-04-24 00:52:55','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(149,18,'2021-05-11 12:16:30','2021-05-11 17:14:26','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(150,18,'2021-05-12 15:57:56','2021-05-13 04:23:49','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(151,20,'2021-05-08 06:52:49','2021-05-09 03:21:09','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(152,20,'2021-04-23 20:50:11','2021-04-24 16:11:59','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(153,20,'2021-05-10 01:12:16','2021-05-10 02:33:13','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(154,20,'2021-04-27 13:18:42','2021-04-27 22:37:30','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(155,20,'2021-04-30 01:15:03','2021-04-30 06:05:13','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(156,20,'2021-05-15 19:13:18','2021-05-16 04:57:55','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(157,20,'2021-04-23 18:22:05','2021-04-24 13:29:20','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(158,20,'2021-05-12 01:03:47','2021-05-12 11:21:09','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(159,20,'2021-04-20 21:24:05','2021-04-21 03:58:25','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(160,20,'2021-04-22 22:01:11','2021-04-23 10:48:41','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(161,21,'2021-05-13 09:04:51','2021-05-13 18:31:25','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(162,21,'2021-05-11 23:53:29','2021-05-12 16:02:51','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(163,21,'2021-05-15 12:25:15','2021-05-16 08:06:32','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(164,21,'2021-05-02 18:43:17','2021-05-02 19:34:26','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(165,21,'2021-04-28 05:36:35','2021-04-28 16:40:34','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(166,21,'2021-04-28 21:52:58','2021-04-29 11:24:18','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(167,21,'2021-04-20 16:58:10','2021-04-21 05:55:28','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(168,21,'2021-05-12 10:55:09','2021-05-12 17:16:01','2021-05-20 00:51:54','2021-05-20 00:51:54',NULL),(169,21,'2021-05-16 05:37:15','2021-05-16 14:33:14','2021-05-20 00:51:55','2021-05-20 00:51:55',NULL),(170,21,'2021-05-14 17:13:38','2021-05-15 03:45:38','2021-05-20 00:51:55','2021-05-20 00:51:55',NULL),(171,22,'2021-05-13 12:13:50','2021-05-13 19:44:23','2021-05-20 00:51:55','2021-05-20 00:51:55',NULL),(172,22,'2021-05-02 14:30:54','2021-05-02 20:40:04','2021-05-20 00:51:55','2021-05-20 00:51:55',NULL),(173,22,'2021-05-03 00:55:27','2021-05-03 23:38:26','2021-05-20 00:51:55','2021-05-20 00:51:55',NULL),(174,22,'2021-05-06 13:49:48','2021-05-06 18:36:05','2021-05-20 00:51:55','2021-05-20 00:51:55',NULL),(175,22,'2021-05-15 16:29:02','2021-05-16 16:12:48','2021-05-20 00:51:55','2021-05-20 00:51:55',NULL),(176,22,'2021-05-05 21:12:17','2021-05-06 07:51:18','2021-05-20 00:51:55','2021-05-20 00:51:55',NULL),(177,22,'2021-04-29 14:25:50','2021-04-29 22:41:33','2021-05-20 00:51:55','2021-05-20 00:51:55',NULL),(178,22,'2021-05-08 07:24:01','2021-05-08 19:59:08','2021-05-20 00:51:55','2021-05-20 00:51:55',NULL),(179,22,'2021-05-10 02:28:59','2021-05-10 13:16:04','2021-05-20 00:51:55','2021-05-20 00:51:55',NULL),(180,22,'2021-05-08 10:59:18','2021-05-09 08:56:23','2021-05-20 00:51:55','2021-05-20 00:51:55',NULL),(181,23,'2021-05-04 04:29:52','2021-05-04 19:48:09','2021-05-20 00:51:55','2021-05-20 00:51:55',NULL),(182,23,'2021-05-08 19:27:59','2021-05-09 16:23:17','2021-05-20 00:51:55','2021-05-20 00:51:55',NULL),(183,23,'2021-05-07 15:02:19','2021-05-08 05:28:34','2021-05-20 00:51:55','2021-05-20 00:51:55',NULL),(184,23,'2021-05-01 22:52:49','2021-05-02 20:08:27','2021-05-20 00:51:55','2021-05-20 00:51:55',NULL),(185,23,'2021-04-19 03:21:37','2021-04-19 18:57:06','2021-05-20 00:51:55','2021-05-20 00:51:55',NULL),(186,23,'2021-04-23 18:09:18','2021-04-24 00:27:57','2021-05-20 00:51:55','2021-05-20 00:51:55',NULL),(187,23,'2021-04-30 18:10:32','2021-05-01 10:11:48','2021-05-20 00:51:55','2021-05-20 00:51:55',NULL),(188,23,'2021-05-15 10:01:43','2021-05-15 18:43:47','2021-05-20 00:51:55','2021-05-20 00:51:55',NULL),(189,23,'2021-04-20 10:42:43','2021-04-21 00:14:36','2021-05-20 00:51:55','2021-05-20 00:51:55',NULL),(190,23,'2021-05-14 19:09:30','2021-05-14 23:01:33','2021-05-20 00:51:55','2021-05-20 00:51:55',NULL),(191,24,'2021-05-09 12:01:42','2021-05-09 23:21:29','2021-05-20 00:51:55','2021-05-20 00:51:55',NULL),(192,24,'2021-05-18 12:47:32','2021-05-19 03:00:20','2021-05-20 00:51:55','2021-05-20 00:51:55',NULL),(193,24,'2021-05-11 23:43:15','2021-05-12 22:07:36','2021-05-20 00:51:55','2021-05-20 00:51:55',NULL),(194,24,'2021-05-04 07:20:21','2021-05-05 01:56:28','2021-05-20 00:51:55','2021-05-20 00:51:55',NULL),(195,24,'2021-05-08 13:00:30','2021-05-08 22:32:06','2021-05-20 00:51:55','2021-05-20 00:51:55',NULL),(196,24,'2021-04-25 03:58:15','2021-04-25 18:30:58','2021-05-20 00:51:55','2021-05-20 00:51:55',NULL),(197,24,'2021-05-01 14:29:49','2021-05-01 15:06:29','2021-05-20 00:51:55','2021-05-20 00:51:55',NULL),(198,24,'2021-05-15 19:43:31','2021-05-15 20:52:34','2021-05-20 00:51:55','2021-05-20 00:51:55',NULL),(199,24,'2021-05-02 07:52:15','2021-05-02 22:14:26','2021-05-20 00:51:55','2021-05-20 00:51:55',NULL),(200,24,'2021-04-19 03:28:26','2021-04-19 16:59:51','2021-05-20 00:51:55','2021-05-20 00:51:55',NULL);
/*!40000 ALTER TABLE `appointments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `media` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint unsigned NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `collection_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `conversions_disk` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` bigint unsigned NOT NULL,
  `manipulations` json NOT NULL,
  `custom_properties` json NOT NULL,
  `generated_conversions` json NOT NULL,
  `responsive_images` json NOT NULL,
  `order_column` int unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `media_uuid_unique` (`uuid`),
  KEY `media_model_type_model_id_index` (`model_type`,`model_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media`
--

LOCK TABLES `media` WRITE;
/*!40000 ALTER TABLE `media` DISABLE KEYS */;
/*!40000 ALTER TABLE `media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_100000_create_password_resets_table',1),(2,'2019_08_19_000000_create_failed_jobs_table',1),(3,'2021_04_26_121834_create_media_table',1),(4,'2021_04_26_122445_create_permission_tables',1),(5,'2021_04_26_122851_create_users_table',1),(6,'2021_05_17_205628_add_registered_by_to_users_table',1),(7,'2021_05_18_203805_create_appointments_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_permissions`
--

DROP TABLE IF EXISTS `model_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `model_has_permissions` (
  `permission_id` bigint unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_permissions`
--

LOCK TABLES `model_has_permissions` WRITE;
/*!40000 ALTER TABLE `model_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_roles`
--

DROP TABLE IF EXISTS `model_has_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `model_has_roles` (
  `role_id` bigint unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_roles`
--

LOCK TABLES `model_has_roles` WRITE;
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;
INSERT INTO `model_has_roles` VALUES (1,'App\\Models\\User',1),(2,'App\\Models\\User',2),(2,'App\\Models\\User',3),(2,'App\\Models\\User',4),(2,'App\\Models\\User',5),(2,'App\\Models\\User',6),(1,'App\\Models\\User',7),(2,'App\\Models\\User',8),(2,'App\\Models\\User',9),(2,'App\\Models\\User',10),(2,'App\\Models\\User',11),(2,'App\\Models\\User',12),(1,'App\\Models\\User',13),(2,'App\\Models\\User',14),(2,'App\\Models\\User',15),(2,'App\\Models\\User',16),(2,'App\\Models\\User',17),(2,'App\\Models\\User',18),(1,'App\\Models\\User',19),(2,'App\\Models\\User',20),(2,'App\\Models\\User',21),(2,'App\\Models\\User',22),(2,'App\\Models\\User',23),(2,'App\\Models\\User',24);
/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permissions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_has_permissions`
--

DROP TABLE IF EXISTS `role_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role_has_permissions` (
  `permission_id` bigint unsigned NOT NULL,
  `role_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_has_permissions`
--

LOCK TABLES `role_has_permissions` WRITE;
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin','Administrador','web','2021-05-20 00:51:50','2021-05-20 00:51:50'),(2,'employee','Funcionário','web','2021-05-20 00:51:50','2021-05-20 00:51:50');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `registered_by` bigint unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cpf` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` datetime NOT NULL,
  `zipcode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `neighborhood` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_cpf_unique` (`cpf`),
  KEY `users_registered_by_foreign` (`registered_by`),
  CONSTRAINT `users_registered_by_foreign` FOREIGN KEY (`registered_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,NULL,'Main Admin','main@admin.com','$2y$10$zDCvjQpQePMj1qwHK0RxZe7MhVVJO.I/xynnOdDu6rw3AOZ/g9qT2','756.470.600-72','Gestor Principal','1994-03-18 00:00:00','37505-166','Rua Reverendo Moisés Pinto Ribeiro','249','Vila Rubens','Itajubá','MG',1,NULL,'2021-05-20 00:51:50','2021-05-20 00:51:50','2021-05-20 00:51:50',NULL,NULL),(2,1,'Vitor Emílio Zaragoça Neto','dayane.pontes@r7.com','$2y$10$GZqh7Cvi6XSqRhxDRF3oFueWriXJiIqkNnnbL.kzVSUyW2K/4TwhS','342.060.478-55','Aliquid','1992-09-04 00:00:00','42817-767','R. Juliana Beltrão','55604','Vila Joaquin','Vila Emanuel do Norte','RN',1,NULL,NULL,'2021-05-20 00:51:51','2021-05-20 00:51:51',NULL,NULL),(3,1,'Sr. Sergio Romero','walter.romero@uol.com.br','$2y$10$6uQW0MnkWuduxXoebK38NemJaFfC67Llv5/M8e3OvYW25FKPmsxO2','622.231.106-73','Inventore','1984-05-23 00:00:00','51925-792','Rua Paula Alcantara','288','Vila Otávio','Benjamin d\'Oeste','DF',1,NULL,NULL,'2021-05-20 00:51:51','2021-05-20 00:51:51',NULL,NULL),(4,1,'Samanta Carmona Leal Jr.','deverso.samara@hotmail.com','$2y$10$ytrHCytNY6xxwk.oavlbI.IsqgMN1s7fubXVtZtZ83DjYLJpkOnyS','783.961.665-87','Sequi','1973-08-12 00:00:00','98823-399','Rua Tábata Ortiz','16794','Vila Cláudio','Matias do Leste','RO',1,NULL,NULL,'2021-05-20 00:51:51','2021-05-20 00:51:51',NULL,NULL),(5,1,'Emerson Ferraz Saraiva','odelvalle@terra.com.br','$2y$10$ht4cK6cOy/7LmnEZkd8f/OL3IywOcaMKG6P9bgRToAwaOHS/7sK6W','212.570.053-08','Et','1981-01-27 00:00:00','20939-928','Av. Casanova','3','Vila Fátima','Chaves do Norte','RJ',1,NULL,NULL,'2021-05-20 00:51:51','2021-05-20 00:51:51',NULL,NULL),(6,1,'Cauan Furtado Santacruz Jr.','flor.davila@r7.com','$2y$10$Diso6e.s.PwtXMqIjtYyhu3kqBcAlqXkvGJObnwGTf8wuOStahmZ6','133.876.470-50','Nihil','1995-05-30 00:00:00','61903-195','R. Yuri Camacho','2','Vila Julieta','Vieira do Sul','AP',1,NULL,NULL,'2021-05-20 00:51:51','2021-05-20 00:51:51',NULL,NULL),(7,1,'Dr. Fernando de Souza','jfranco@hotmail.com','$2y$10$lLB.7V1DrVEWikgRD9rICeJFC.bjNBn66T/wY8VxE4/B44tzn8ePC','706.559.058-55','Perferendis','1982-07-17 00:00:00','80939-337','Rua Valéria Franco','85','Vila Márcio','Antônio do Leste','MG',1,NULL,NULL,'2021-05-20 00:51:52','2021-05-20 00:51:52',NULL,NULL),(8,7,'Dr. Juan Valdez Jr.','erios@gmail.com','$2y$10$SibQAwEIJ0r3x4FmybG5vu6sdY84gPA5qEl4BQ5NlfFGrgSHVvMvq','257.596.885-29','Enim','1971-05-22 00:00:00','16586-566','R. Dias','53','Vila Jéssica','Flores do Norte','RS',1,NULL,NULL,'2021-05-20 00:51:52','2021-05-20 00:51:52',NULL,NULL),(9,7,'Norma Alves Filho','agostinho55@ig.com.br','$2y$10$uXxZEN3sZ2uqPZ.KQavvyeeBUCggzgdC/WbnZwMUgBzsKm/8UpuPK','291.627.665-38','Modi','1981-10-02 00:00:00','00881-523','Rua Jean','153','Vila David','São Beatriz','CE',1,NULL,NULL,'2021-05-20 00:51:52','2021-05-20 00:51:52',NULL,NULL),(10,7,'Dr. Jorge Souza Lira Neto','janaina.delatorre@uol.com.br','$2y$10$4XXDEtjCE6tjSL7H5YDB2.dLeN9TOzA8fKjwfnYJYsEsjp.GEh5Hu','693.944.380-09','Et','1994-02-13 00:00:00','46770-126','Avenida Lidiane Padilha','896','Vila Cynthia','Porto Ariana do Leste','TO',1,NULL,NULL,'2021-05-20 00:51:52','2021-05-20 00:51:52',NULL,NULL),(11,7,'Estêvão Santos Maia','fabiana.ferreira@ig.com.br','$2y$10$BWcebYvZaRw4BPWB4N2q4uO9L2XOzTzuf0G3LcFFVbFYr86NWXV/6','980.917.511-68','Qui','1994-11-21 00:00:00','68758-076','Avenida Jonas Bezerra','813','Vila Heloísa','Porto Samuel','PE',1,NULL,NULL,'2021-05-20 00:51:53','2021-05-20 00:51:53',NULL,NULL),(12,7,'Dr. Suzana Amanda Lira','horacio39@uol.com.br','$2y$10$rdg2p4buIiyu7JKdZrm2YOGQHuVoawU6Kjai5nlqsZrCiWDhEmSFe','839.272.576-05','Consequatur','1971-03-17 00:00:00','20549-944','Rua Milene Lutero','92483','Vila Estêvão','Simão do Leste','RJ',1,NULL,NULL,'2021-05-20 00:51:53','2021-05-20 00:51:53',NULL,NULL),(13,1,'Sofia Viviane Serra','dprado@ig.com.br','$2y$10$48VT4pjNnxE91pUF3bxsLuOuTuVY7F.jTO6sXGdUJ2MPybgsWyneG','675.796.375-29','Porro','1995-03-11 00:00:00','62618-483','Avenida Ingrid Lourenço','8703','Vila Ítalo','São Ian','RJ',1,NULL,NULL,'2021-05-20 00:51:53','2021-05-20 00:51:53',NULL,NULL),(14,13,'Dr. Pedro Tomás Valdez','rodrigo66@ig.com.br','$2y$10$NwvLdQM9UiCWuCg2SE/TFOVNbzo5JibBYoWehtrMAiypkHiqtBhkW','427.941.047-05','Necessitatibus','1983-02-04 00:00:00','19590-520','R. Máximo','3','Vila Cláudia','Franco do Sul','TO',1,NULL,NULL,'2021-05-20 00:51:53','2021-05-20 00:51:53',NULL,NULL),(15,13,'Leo Valentin Filho','ggil@uol.com.br','$2y$10$m4r1aC8oO7AtXD.QB0HIve1uJPGAM3XXzAfQzjem1nSu0YJk.gOiG','226.984.338-09','Similique','1978-05-19 00:00:00','89495-869','Rua Sérgio Gomes','1','Vila Gilberto','São Pedro','MS',1,NULL,NULL,'2021-05-20 00:51:53','2021-05-20 00:51:53',NULL,NULL),(16,13,'Cláudio da Silva','ialves@yahoo.com','$2y$10$Ln9Cqbb2PDW23ddJz9nntu624ftlalJfW991BPZ9CcDsQtbd3u5Oe','753.377.843-07','Quis','1998-02-19 00:00:00','24160-924','Av. Renato Padilha','88401','Vila Dirce','Vila Matias do Sul','SP',1,NULL,NULL,'2021-05-20 00:51:53','2021-05-20 00:51:53',NULL,NULL),(17,13,'Beatriz Isis Godói Sobrinho','vila.rafaela@yahoo.com','$2y$10$tDX7rqw1krZauueSzz8LLOwLjDCsCLtdocj9d3ZqD0y4myRToccrC','783.278.072-00','Consequatur','1992-09-16 00:00:00','11577-625','Avenida Edilson Santana','358','Vila Constância','Salas do Sul','MT',1,NULL,NULL,'2021-05-20 00:51:54','2021-05-20 00:51:54',NULL,NULL),(18,13,'Théo Heitor Marinho Filho','vanessa59@ig.com.br','$2y$10$NfHPFunMyMOBZEmCrMLwruY0XYOGlRT8QK5wrLa2Gjb8.29.iCmmW','112.746.572-45','Impedit','1994-11-06 00:00:00','32532-684','R. Catarina','83','Vila Juan','Serra d\'Oeste','PA',1,NULL,NULL,'2021-05-20 00:51:54','2021-05-20 00:51:54',NULL,NULL),(19,1,'Hosana Camacho Lovato','kramos@yahoo.com','$2y$10$UCFeYpYEO6VRtFpsI/103Oi4NLBRfGocz3.DfgjohbinmCeRexDy.','432.545.747-02','Ea','1982-10-12 00:00:00','22314-632','Largo Pablo Perez','9','Vila Aaron','Ferminiano do Norte','MG',1,NULL,NULL,'2021-05-20 00:51:54','2021-05-20 00:51:54',NULL,NULL),(20,19,'Luara Branco Neto','vanessa.toledo@hotmail.com','$2y$10$iclweTEM.67GUk5xNUDCque6FPMHy2QRXrj.53vbaZ99ySoCjXJoC','330.571.433-69','Illum','1993-01-22 00:00:00','74124-332','Rua Oliveira','64','Vila Sebastião','Santa Joaquim','PI',1,NULL,NULL,'2021-05-20 00:51:54','2021-05-20 00:51:54',NULL,NULL),(21,19,'Robson Dante Matias Sobrinho','branco.adriana@terra.com.br','$2y$10$3xxe1iH87JCMOlZaoKEIVu5bprGJu15sBD.8HV50ecrxmjNkqLgR6','287.745.047-38','Fugiat','1976-05-01 00:00:00','90915-618','Av. Vale','2246','Vila Carol','Medina d\'Oeste','SE',1,NULL,NULL,'2021-05-20 00:51:54','2021-05-20 00:51:54',NULL,NULL),(22,19,'Dr. Afonso Gonçalves','deivid12@uol.com.br','$2y$10$G0A2K1mhkq0Odtea1Zi9V.KdcqwYYeqKf6Ea3ZStqMlIptCmlFZJ2','518.703.744-79','Nulla','1988-07-24 00:00:00','95674-820','Avenida Taís','39903','Vila Dayane','Porto Liz','ES',1,NULL,NULL,'2021-05-20 00:51:55','2021-05-20 00:51:55',NULL,NULL),(23,19,'Maximiano Molina','lucas.serna@ig.com.br','$2y$10$uDZ502eUZEgcbiK7.sOHuO0NZA5qb2T4jpM3FnqyWGnl/0C6YPDeG','939.445.038-64','Nulla','1983-03-13 00:00:00','30122-835','Largo Amaral','1','Vila Miranda','Fonseca do Sul','SP',1,NULL,NULL,'2021-05-20 00:51:55','2021-05-20 00:51:55',NULL,NULL),(24,19,'Dr. Daniel Ronaldo Corona Sobrinho','xdarosa@r7.com','$2y$10$eSCb/OUlU234eEwkG8ZdHeulmzWxE.XcuYHWpqnFGLQ0uQFxtirXW','074.789.264-44','Minima','1998-12-11 00:00:00','26615-747','Av. Daniel','8141','Vila Katherine','Prado d\'Oeste','MG',1,NULL,NULL,'2021-05-20 00:51:55','2021-05-20 00:51:55',NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-19 21:59:52
