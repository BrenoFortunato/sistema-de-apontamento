<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("users", function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->string("name");
            $table->string("email")->unique();
            $table->string("password");
            $table->string("cpf")->unique();
            $table->string("position");
            $table->datetime("birthday");
            $table->string("zipcode");
            $table->string("address");
            $table->string("number");
            $table->string("neighborhood");
            $table->string("city");
            $table->string("state");
            $table->boolean("is_active")->default(true);
            $table->string("photo")->nullable();
            $table->timestamp("email_verified_at")->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->rememberToken();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("users");
    }
}
