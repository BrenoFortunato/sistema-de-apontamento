<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("appointments", function (Blueprint $table) {
            $table->increments("id");
            $table->unsignedBigInteger("user_id")->unsigned();
            $table->foreign("user_id")->references("id")->on("users");
            $table->datetime("start_time");
            $table->datetime("end_time")->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("appointments");
    }
}
