<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Appointment;
use Illuminate\Database\Seeder;

class DummySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create 5 employees attached to the Main Admin (created on SetupSeeder)
        User::factory()
            ->count(5)
            ->employee()
            ->has(Appointment::factory()->count(10), "appointments")
            ->create([
                "registered_by" => 1
            ]);

        // Create 3 admin users attached to the Main Admin
        // Foreach admin created, creates 5 employees attached to them
        // Foreach employee created, creates 10 appointments attached to them
        User::factory()
            ->count(3)
            ->admin()
            ->has(User::factory()->count(5)->employee()->has(Appointment::factory()->count(10), "appointments"), "registeredUsers")
            ->create([
                "registered_by" => 1
            ]);
    }
}
