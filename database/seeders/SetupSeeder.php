<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class SetupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currentTimestamp = DB::raw("CURRENT_TIMESTAMP");

        // Role
        $roles = array_map(function ($data) use ($currentTimestamp) {
            return array_merge($data, [
                "created_at" => $currentTimestamp,
                "updated_at" => $currentTimestamp,
            ]);
        }, array_values(config("enums.roles")));
        DB::table("roles")->insert($roles);

        // Admin
        $users = [
            [
                "name"              => "Main Admin",
                "email"             => "main@admin.com",
                "password"          => bcrypt("123456"),
                "cpf"               => "756.470.600-72",
                "position"          => "Gestor Principal",
                "birthday"          => "1994-03-18 00:00:00",
                "zipcode"           => "37505-166",
                "address"           => "Rua Reverendo Moisés Pinto Ribeiro",
                "number"            => "249",
                "neighborhood"      => "Vila Rubens",
                "city"              => "Itajubá",
                "state"             => "MG",
                "is_active"         => true,
                "email_verified_at" => $currentTimestamp,
                "created_at"        => $currentTimestamp,
                "updated_at"        => $currentTimestamp,
            ],
        ];
        DB::table("users")->insert($users);

        // Admin Role
        $userRoles = [
            [
                "model_type" => "App\Models\User",
                "role_id"    => config("enums.roles.ADMIN.id"),
                "model_id"   => 1
            ],
        ];
        DB::table("model_has_roles")->insert($userRoles);
    }
}
