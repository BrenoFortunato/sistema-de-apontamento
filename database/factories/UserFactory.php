<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "registered_by"     => $this->faker->randomDigitNotNull,
            "name"              => $this->faker->name,
            "email"             => $this->faker->freeEmail,
            "password"          => "123456",
            "cpf"               => $this->faker->cpf,
            "position"          => ucfirst($this->faker->jobTitle),
            "birthday"          => $this->faker->date("Y-m-d 00:00:00", "2000-01-01 00:00:00"),
            "zipcode"           => $this->faker->postcode,
            "address"           => $this->faker->streetName,
            "number"            => $this->faker->buildingNumber,
            "neighborhood"      => "Vila ".$this->faker->firstName,
            "city"              => $this->faker->city,
            "state"             => $this->faker->stateAbbr,
            "is_active"         => true,
            "photo"             => null,
            "email_verified_at" => null,
        ];
    }

    /**
     * Indicate that the user is admin.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function admin()
    {
        return $this->afterCreating(function (User $user) {
            $user->assignRole(config("enums.roles.ADMIN.name"));
        });
    }

    /**
     * Indicate that the user is employee.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function employee()
    {
        return $this->afterCreating(function (User $user) {
            $user->assignRole(config("enums.roles.EMPLOYEE.name"));
        });
    }
}
