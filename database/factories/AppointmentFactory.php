<?php

namespace Database\Factories;

use Carbon;
use App\Models\Appointment;
use Illuminate\Database\Eloquent\Factories\Factory;

class AppointmentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Appointment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $startTime = Carbon::today()->startOfDay()->subDays(rand(1, 30))->setHours(rand(0, 23))->setMinutes(rand(0, 59))->setSeconds(rand(0, 59));
        return [
            "user_id"    => $this->faker->randomDigitNotNull,
            "start_time" => $startTime,
            "end_time"   => $startTime->copy()->addHours(rand(0, 23))->addMinutes(rand(0, 59))->addSeconds(rand(0, 59)),
        ];
    }
}
