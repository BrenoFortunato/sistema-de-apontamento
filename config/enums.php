<?php

return [

    "roles" => [
        "ADMIN"            => ["id" => 1, "name" => "admin",         "display_name" => "Administrador",            "guard_name" => "web"],
        "EMPLOYEE"         => ["id" => 2, "name" => "employee",      "display_name" => "Funcionário",              "guard_name" => "web"],
    ],

];
