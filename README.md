# Sistema de Apontamento
Projeto desenvolvido em Docker utilizando PHP v7.4, MySQL v8.0.23, Laravel v8.39 e AdminLTE v3.0.5.
  
## Utilização
Após clonar este repositório, abra o projeto, duplique o arquivo **.env.example** e renomeie-o para **.env**. Adicione o endereço de APP_URL (sem http) ao arquivo de hosts do sistema operacional, da seguinte forma:
```php
127.0.0.1           dev.appointment-system.com.br
```

Para **iniciar o Docker**, abra um terminal na pasta do projeto e execute o comando abaixo. Deixe este terminal executando enquanto estiver utilizando o projeto:
```php
make up
```

Para acessar o **bash do Docker**, abra um novo terminal na pasta do projeto e execute o comando abaixo. Todos os comandos do Laravel ("php artisan", "composer update", etc.) devem ser executados dentro deste bash:
```php
make sh
```

Ao subir o projeto pela primeira vez, execute as migrations e os seeders utilizando o comando a seguir **dentro do bash** anterior:
```php
php artisan migrate --seed
```

Para acessar o **banco do Docker**, abra um novo terminal na pasta do projeto e execute:
```php
make sh:db
```

## Banco de Dados
O comando executado anteriormente ("php artisan migrate --seed") já irá criar o banco e populá-lo. Não há necessidade, mas caso deseje, foi disponibilizado um dump com o nome **BANCODEDADOS.sql**. Para restaurá-lo basta abrir um terminal na raiz do projeto e executar o comando abaixo:
```php
mysql -uroot -p'root' -h127.0.0.1 --port=33306 appointment_system < BANCODEDADOS.sql
```

Para realizar um novo dump, basta executar o comando abaixo na pasta onde desejar que o arquivo seja salvo:
```php
mysqldump -uroot -p'root' -h127.0.0.1 --port=33306 appointment_system > BANCODEDADOS.sql
```

Nenhum desses comandos devem ser executados dentro do bash do docker ("make sh").