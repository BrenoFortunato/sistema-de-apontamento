<?php

namespace App\Http\Controllers;

use Carbon;
use Lang;
use Flash;
use Response;
use App\DataTables\AppointmentDataTable;
use App\Repositories\AppointmentRepository;
use App\Http\Controllers\AppBaseController;

class AppointmentController extends AppBaseController
{
    private $appointmentRepository;

    /**
     * Create a new controller instance.
     * 
     * @param AppointmentRepository $appointmentRepo
     *
     * @return void
     */
    public function __construct(AppointmentRepository $appointmentRepo)
    {
        $this->appointmentRepository = $appointmentRepo;
    }

    /**
     * Display a listing of the Appointment.
     *
     * @param AppointmentDataTable $appointmentDataTable
     *
     * @return Response
     */
    public function index(AppointmentDataTable $appointmentDataTable)
    {
        return $appointmentDataTable->render("appointments.index");
    }

    /**
     * End the specified Appointment.
     *
     * @return Response
     */
    public function end()
    {
        $appointment = $this->appointmentRepository->find(request()->appointment_id);
        if (empty($appointment)) {
            Flash::error(Lang::choice("tables.appointments", "s")." ".Lang::choice("flash.not_found", "m"));
            return redirect(route("appointments.index"));
        }

        $input = [
            "end_time" => Carbon::now()
        ];
        $appointment = $this->appointmentRepository->update($input, $appointment->id);

        Flash::success(Lang::choice("tables.appointments", "s")." ".Lang::choice("flash.finished", "m"));
        return redirect(route("home"));
    }
}
