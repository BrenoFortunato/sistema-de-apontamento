<?php

namespace App\Http\Controllers;

use Cep;
use Illuminate\Routing\Controller;

class AddressController extends Controller
{
    /**
     * Find address by zipcode
     *
     * @return json
    */
    public function findByZipcode($zipcode)
    {
        $cep = Cep::find($zipcode);
        $address = $cep->getCepModel();

        return json_encode($address);
    }
}
