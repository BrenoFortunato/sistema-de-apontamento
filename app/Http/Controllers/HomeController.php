<?php

namespace App\Http\Controllers;

use Auth;
use Flash;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\Request::routeIs(["home"])) {
            $user = Auth::user();
            if ($user->hasRole([config("enums.roles.EMPLOYEE.name")])) {
                $lastAppointment = $user->appointments->sortByDesc("created_at")->first();
            } else {
                $lastAppointment = null;
            }
            return response(view("home", compact("lastAppointment")));
        } else {
            return redirect(route("home"));
        }
    }
}
