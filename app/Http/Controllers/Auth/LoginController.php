<?php

namespace App\Http\Controllers\Auth;

use Carbon;
use App\Models\Appointment;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware("guest")->except("logout");
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * 
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        if ($user->hasRole([config("enums.roles.EMPLOYEE.name")])) {
            Appointment::create([
                "user_id"    => $user->id,
                "start_time" => Carbon::now(),
            ]);
        }
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * 
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        $user = $this->guard()->user();
        if ($user->hasRole([config("enums.roles.EMPLOYEE.name")])) {
            $lastAppointment = $user->appointments->sortByDesc("created_at")->first();
            if (empty($lastAppointment->end_time)) {
                $lastAppointment->end_time = Carbon::now();
                $lastAppointment->save();
            }
        }

        $this->guard()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        if ($response = $this->loggedOut($request)) {
            return $response;
        }

        return $request->wantsJson()
            ? new JsonResponse([], 204)
            : redirect('/');
    }
}
