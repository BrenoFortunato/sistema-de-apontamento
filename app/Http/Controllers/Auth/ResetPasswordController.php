<?php

namespace App\Http\Controllers\Auth;

use Carbon;
use App\Models\Appointment;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Str;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            "token"    => "required",
            "email"    => "required|string|email|max:255",
            "password" => "required|string|min:6|confirmed",
        ];
    }

    /**
     * Set the user's password.
     *
     * @param  mixed   $user
     * @param  string  $password
     * 
     * @return void
     */
    protected function setUserPassword($user, $password)
    {
        $user->password = $password;
    }

    /**
     * Reset the given user's password.
     *
     * @param  mixed   $user
     * @param  string  $password
     * 
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        $this->setUserPassword($user, $password);

        $user->setRememberToken(Str::random(60));

        $user->save();

        event(new PasswordReset($user));

        $this->guard()->login($user);

        if ($user->hasRole([config("enums.roles.EMPLOYEE.name")])) {
            Appointment::create([
                "user_id"    => $user->id,
                "start_time" => Carbon::now(),
            ]);
        }
    }
}
