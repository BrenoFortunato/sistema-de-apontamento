<?php

namespace App\Repositories;

use App\Models\Appointment;
use App\Repositories\BaseRepository;

/**
 * Class AppointmentRepository
 * @package App\Repositories
 * @version May 18, 2021, 8:38 pm -03
*/
class AppointmentRepository extends BaseRepository
{
    /**
     * The fields that can be searched.
     *
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'start_time',
        'end_time',
    ];

    /**
     * Return searchable fields.
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model.
     **/
    public function model()
    {
        return Appointment::class;
    }
}
