<?php

namespace App\DataTables;

use DB;
use Lang;
use App\Models\User;
use App\Services\DataTablesDefaults;
use Yajra\DataTables\Datatables;
use Yajra\DataTables\Services\DataTable;

class UserDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\DataTables\Datatables
     */
    public function dataTable()
    {
        $users = User::select(
            "users.*",
            DB::raw("(
                        SELECT roles.display_name
                        FROM roles
                        LEFT JOIN model_has_roles ON model_has_roles.role_id = roles.id
                        WHERE model_has_roles.model_id = users.id
                    ) as readable_role_name"),
            DB::raw("(CASE
                        WHEN users.is_active=true  THEN 'Sim'
                        WHEN users.is_active=false THEN 'Não'
                        ELSE NULL
                    END) as readable_is_active"),    
            DB::raw("(
                        SELECT u.name
                        FROM users as u
                        WHERE u.id = users.registered_by
                    ) as registered_by_name"),
            DB::raw("CONCAT(DATE_FORMAT(users.birthday, '%d/%m/%Y'), ' (',
                        CONCAT(TIMESTAMPDIFF(YEAR, users.birthday, CURDATE()), ' anos)')
                    ) as readable_birthday_with_age"),
            DB::raw("CONCAT(users.address, ', Nº ',
                        CONCAT(users.number, ' - ',
                            CONCAT(users.neighborhood, ' - ',
                                CONCAT(users.city, '/',
                                    CONCAT(users.state, ' (',
                                        CONCAT(users.zipcode, ')')
                                    )
                                )
                            )
                        )
                    ) as full_address"),
        );

        // Filter contacted_user
        if (request()->deleted_users) {
            $users = $users->onlyTrashed();
        }

        return DataTables::of($users)
            ->filterColumn("readable_role_name", function ($query, $keyword) {
                $query->whereRaw("(
                                    SELECT roles.display_name
                                    FROM roles
                                    LEFT JOIN model_has_roles ON model_has_roles.role_id = roles.id
                                    WHERE model_has_roles.model_id = users.id
                                ) like ?", ["%{$keyword}%"]);
            })
            ->filterColumn("readable_is_active", function ($query, $keyword) {
                $query->whereRaw("(CASE
                                    WHEN users.is_active=true  THEN 'Sim'
                                    WHEN users.is_active=false THEN 'Não'
                                    ELSE NULL
                                END) like ?", ["%{$keyword}%"]);
            })
            ->filterColumn("registered_by_name", function ($query, $keyword) {
                $query->whereRaw("(
                                    SELECT u.name
                                    FROM users as u
                                    WHERE u.id = users.registered_by
                                ) like ?", ["%{$keyword}%"]);
            })
            ->filterColumn("birthday", function ($query, $keyword) {
                $query->whereRaw("CONCAT(DATE_FORMAT(users.birthday, '%d/%m/%Y'), ' (',
                                    CONCAT(TIMESTAMPDIFF(YEAR, users.birthday, CURDATE()), ' anos)')
                                ) like ?", ["%{$keyword}%"]);
            })
            ->filterColumn("full_address", function ($query, $keyword) {
                $query->whereRaw("CONCAT(users.address, ', Nº ',
                                    CONCAT(users.number, ' - ',
                                        CONCAT(users.neighborhood, ' - ',
                                            CONCAT(users.city, '/',
                                                CONCAT(users.state, ' (',
                                                    CONCAT(users.zipcode, ')')
                                                )
                                            )
                                        )
                                    )
                                ) like ?", ["%{$keyword}%"]);
            })
            ->addColumn("action", "users.datatables_actions")
            ->rawColumns(["action"]);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->minifiedAjax()
            ->columns($this->getColumns())
            ->addAction(["width" => "75px", "printable" => false, "title" => Lang::get("datatables.action")])
            ->parameters(DataTablesDefaults::getParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            "id"                          => ["name" => "id",                 "render" => "(data!=null)? ((data.length>180)? data.substr(0,180)+'...' : data) : '-'", "title" => Lang::get("attributes.id")],
            "registered_by_name"          => ["name" => "registered_by_name", "render" => "(data!=null)? ((data.length>180)? data.substr(0,180)+'...' : data) : '-'", "title" => Lang::get("attributes.registered_by")],
            "name"                        => ["name" => "name",               "render" => "(data!=null)? ((data.length>180)? data.substr(0,180)+'...' : data) : '-'", "title" => Lang::get("attributes.name")],
            "email"                       => ["name" => "email",              "render" => "(data!=null)? ((data.length>180)? data.substr(0,180)+'...' : data) : '-'", "title" => Lang::get("attributes.email")],
            "readable_role_name"          => ["name" => "readable_role_name", "render" => "(data!=null)? ((data.length>180)? data.substr(0,180)+'...' : data) : '-'", "title" => Lang::get("attributes.role_name")],
            "cpf"                         => ["name" => "cpf",                "render" => "(data!=null)? ((data.length>180)? data.substr(0,180)+'...' : data) : '-'", "title" => Lang::get("attributes.cpf")],
            "position"                    => ["name" => "position",           "render" => "(data!=null)? ((data.length>180)? data.substr(0,180)+'...' : data) : '-'", "title" => Lang::get("attributes.position"), "visible" => false],
            "readable_birthday_with_age"  => ["name" => "birthday",           "render" => "(data!=null)? ((data.length>180)? data.substr(0,180)+'...' : data) : '-'", "title" => Lang::get("attributes.birthday")],
            "full_address"                => ["name" => "full_address",       "render" => "(data!=null)? ((data.length>180)? data.substr(0,180)+'...' : data) : '-'", "title" => Lang::get("attributes.full_address"), "visible" => false],
            "readable_is_active"          => ["name" => "readable_is_active", "render" => "(data!=null)? ((data.length>180)? data.substr(0,180)+'...' : data) : '-'", "title" => Lang::get("attributes.is_active")],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return Lang::choice("tables.users", "p")." ".date("d.m.Y H\hi\m");
    }
}
