<?php

namespace App\DataTables;

use DB;
use Lang;
use Auth;
use Carbon;
use App\Services\DataTablesDefaults;
use Yajra\DataTables\Datatables;
use Yajra\DataTables\Services\DataTable;

class AppointmentDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\DataTables\Datatables
     */
    public function dataTable()
    {
        $sql = "SELECT
                    appointments.*,
                    users.name                                                as user_name,
                    users.position                                            as user_position,
                    TIMESTAMPDIFF(YEAR, users.birthday, CURDATE())            as user_age,
                    registered_by.name                                        as registered_by_name,
                    DATE_FORMAT(appointments.start_time, '%d/%m/%Y %H:%i:%s') as readable_start_time,
                    DATE_FORMAT(appointments.end_time, '%d/%m/%Y %H:%i:%s')   as readable_end_time,
                    TIMEDIFF(appointments.end_time, appointments.start_time)  as duration
                FROM 
                    appointments
                LEFT JOIN
                    users ON users.id = appointments.user_id
                LEFT JOIN
                    users registered_by ON users.registered_by = registered_by.id
                WHERE
                    registered_by.id = :auth_user
                AND
                    appointments.deleted_at IS NULL";
        $variables["auth_user"] = Auth::user()->id;

        // Filter start_time
        if (request()->start_time) {
            $dateRange = explode(" - ", request()->start_time);
            $variables["start_time_range_0"] = Carbon::createFromFormat("d/m/Y", $dateRange[0])->startOfDay();
            $variables["start_time_range_1"] = Carbon::createFromFormat("d/m/Y", $dateRange[1])->endOfDay();
            $sql .= " AND appointments.start_time BETWEEN :start_time_range_0 AND :start_time_range_1";
        }

        // Filter end_time
        if (request()->end_time) {
            $dateRange = explode(" - ", request()->end_time);
            $variables["end_time_range_0"] = Carbon::createFromFormat("d/m/Y", $dateRange[0])->startOfDay();
            $variables["end_time_range_1"] = Carbon::createFromFormat("d/m/Y", $dateRange[1])->endOfDay();
            $sql .= " AND appointments.end_time BETWEEN :end_time_range_0 AND :end_time_range_1";
        }

        $appointments = DB::select($sql, $variables);

        return DataTables::of($appointments);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->minifiedAjax()
            ->columns($this->getColumns())
            ->parameters(DataTablesDefaults::getParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            "id"                  => ["name" => "id",                 "render" => "(data!=null)? ((data.length>180)? data.substr(0,180)+'...' : data) : '-'", "title" => Lang::get("attributes.id")],
            "user_name"           => ["name" => "user_name",          "render" => "(data!=null)? ((data.length>180)? data.substr(0,180)+'...' : data) : '-'", "title" => Lang::get("attributes.user_id")],
            "user_position"       => ["name" => "user_position",      "render" => "(data!=null)? ((data.length>180)? data.substr(0,180)+'...' : data) : '-'", "title" => Lang::get("attributes.position")],
            "user_age"            => ["name" => "user_age",           "render" => "(data!=null)? ((data.length>180)? data.substr(0,180)+'...' : data) : '-'", "title" => Lang::get("attributes.age")],
            "registered_by_name"  => ["name" => "registered_by_name", "render" => "(data!=null)? ((data.length>180)? data.substr(0,180)+'...' : data) : '-'", "title" => Lang::get("attributes.registered_by")],
            "readable_start_time" => ["name" => "start_time",         "render" => "(data!=null)? ((data.length>180)? data.substr(0,180)+'...' : data) : '-'", "title" => Lang::get("attributes.start_time"), "searchable" => false],
            "start_time_search"   => ["name" => "readable_start_time","render" => "(data!=null)? ((data.length>180)? data.substr(0,180)+'...' : data) : '-'", "title" => Lang::get("attributes.start_time"), "visible" => false, "className" => "colvis-hidden"],
            "readable_end_time"   => ["name" => "end_time",           "render" => "(data!=null)? ((data.length>180)? data.substr(0,180)+'...' : data) : '-'", "title" => Lang::get("attributes.end_time"), "searchable" => false],
            "end_time_search"     => ["name" => "readable_end_time",  "render" => "(data!=null)? ((data.length>180)? data.substr(0,180)+'...' : data) : '-'", "title" => Lang::get("attributes.end_time"), "visible" => false, "className" => "colvis-hidden"],
            "duration"            => ["name" => "duration",           "render" => "(data!=null)? ((data.length>180)? data.substr(0,180)+'...' : data) : '-'", "title" => Lang::get("attributes.duration")],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return Lang::choice("tables.appointments", "p")." ".date("d.m.Y H\hi\m");
    }
}
