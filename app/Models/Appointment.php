<?php

namespace App\Models;

use Eloquent as Model;
use App\Services\Functions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Appointment
 * @package App\Models
 * @version May 18, 2021, 8:38 pm -03
 */
class Appointment extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    public $table = "appointments";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = [
        "user_id",
        "start_time",
        "end_time",
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id"         => "integer",
        "user_id"    => "integer",
        "start_time" => "datetime",
        "end_time"   => "datetime",
    ];

    /**
     * Validation rules.
     *
     * @var array
     */
    public static $rules = [
        "user_id"    => "required",
        "start_time" => "required",
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        "readable_created_at",
        "readable_updated_at",
        "readable_start_time",
        "readable_end_time",
    ];

    // =========================================================================
    // Relationships
    // =========================================================================
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, "user_id", "id");
    }

    // =========================================================================
    // Getters
    // =========================================================================

    /**
     * Get readable_created_at.
     *
     * @return string
     */
    public function getReadableCreatedAtAttribute()
    {
        return Functions::formatDatetime($this->created_at);
    }

    /**
     * Get readable_updated_at.
     *
     * @return string
     */
    public function getReadableUpdatedAtAttribute()
    {
        return Functions::formatDatetime($this->updated_at);
    }

    /**
     * Get readable_start_time.
     *
     * @return string
     */
    public function getReadableStartTimeAttribute()
    {
        return Functions::formatDatetime($this->start_time);
    }

    /**
     * Get readable_end_time.
     *
     * @return string
     */
    public function getReadableEndTimeAttribute()
    {
        return Functions::formatDatetime($this->end_time);
    }
}
