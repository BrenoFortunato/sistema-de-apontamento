<?php

namespace App\Models;

use App\Services\Functions;
use App\Notifications\CustomResetPasswordNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\Permission\Traits\HasRoles;
use Askedio\SoftCascade\Traits\SoftCascadeTrait;

/**
 * Class User
 * @package App\Models
 * @version April 26, 2021, 12:28 pm -03
 */
class User extends Authenticatable implements HasMedia
{
    use HasRoles;
    use HasFactory;
    use Notifiable;
    use InteractsWithMedia;
    use SoftDeletes;
    use SoftCascadeTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    public $table = "users";

    /**
     * The relationships that should be cascade deleted and restored;
     *
     * @var array
     */
    protected $softCascade = [
        "appointments",
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        "password",
        "remember_token",
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "registered_by",
        "name",
        "email",
        "password",
        "cpf",
        "position",
        "birthday",
        "zipcode",
        "address",
        "number",
        "neighborhood",
        "city",
        "state",
        "is_active",
        "photo",
        "email_verified_at",
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id"                => "integer",
        "registered_by"     => "integer",
        "name"              => "string",
        "email"             => "string",
        "password"          => "string",
        "cpf"               => "string",
        "position"          => "string",
        "birthday"          => "datetime",
        "zipcode"           => "string",
        "address"           => "string",
        "number"            => "string",
        "neighborhood"      => "string",
        "city"              => "string",
        "state"             => "string",
        "is_active"         => "boolean",
        "photo"             => "string",
        "email_verified_at" => "datetime",
    ];

    /**
     * Validation rules.
     *
     * @var array
     */
    public static $rules = [
        "name"         => "required|string|max:255",
        "email"        => "required|string|email|max:255|unique:users,email,id_to_ignore,id",
        "password"     => "required|string|min:6|confirmed",
        "role_name"    => "required",
        "cpf"          => "required|string|max:255|unique:users,cpf,id_to_ignore,id",
        "position"     => "required|string|max:255",
        "birthday"     => "required",
        "zipcode"      => "required|string|max:255",
        "address"      => "required|string|max:255",
        "number"       => "required|string|max:255",
        "neighborhood" => "required|string|max:255",
        "city"         => "required|string|max:255",
        "state"        => "required|string|max:255",
        "is_active"    => "required",
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        "readable_created_at",
        "readable_updated_at",
        "readable_email_verified_at",
        "readable_birthday",
        "readable_is_active",
        "readable_role_name",
        "role_name",
    ];

    // =========================================================================
    // Relationships
    // =========================================================================
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function registeredBy()
    {
        return $this->belongsTo(User::class, "registered_by", "id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function registeredUsers()
    {
        return $this->hasMany(User::class, "registered_by");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function appointments()
    {
        return $this->hasMany(Appointment::class, "user_id");
    }

    // =========================================================================
    // Getters
    // =========================================================================

    /**
     * Get user photo or a default photo.
     *
     * @return string
     */
    public function getPhotoAttribute()
    {
        return $this->attributes["photo"]?: asset("images/no_user.png");
    }

    /**
     * Get readable_created_at.
     *
     * @return string
     */
    public function getReadableCreatedAtAttribute()
    {
        return Functions::formatDatetime($this->created_at);
    }

    /**
     * Get readable_updated_at.
     *
     * @return string
     */
    public function getReadableUpdatedAtAttribute()
    {
        return Functions::formatDatetime($this->updated_at);
    }

    /**
     * Get readable_email_verified_at.
     *
     * @return string
     */
    public function getReadableEmailVerifiedAtAttribute()
    {
        return Functions::formatDatetime($this->email_verified_at);
    }

    /**
     * Get readable_birthday.
     *
     * @return string
     */
    public function getReadableBirthdayAttribute()
    {
        return Functions::formatDate($this->birthday);
    }

    /**
     * Get readable_is_active.
     *
     * @return string
     */
    public function getReadableIsActiveAttribute()
    {
        return Functions::formatBoolean($this->is_active);
    }

    /**
     * Get role_name.
     *
     * @return string
     */
    public function getRoleNameAttribute()
    {
        return $this->roles->first()->name;
    }

    /**
     * Get readable_role_name.
     *
     * @return string
     */
    public function getReadableRoleNameAttribute()
    {
        return $this->roles->first()->display_name;
    }

    // =========================================================================
    // Setters
    // =========================================================================

    /**
     * Set password.
     *
     * @param string $value
     *
     * @return void
     */
    public function setPasswordAttribute($value)
    {
        $value? $this->attributes["password"] = bcrypt($value) : null;
    }

    // =========================================================================
    // Methods
    // =========================================================================

    /**
     * Check whether user photo is default or not.
     *
     * @return boolean
     */
    public function isPhotoDefault()
    {
        return $this->attributes["photo"]? false : true;
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     *
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new CustomResetPasswordNotification($token));
    }

    /**
     * File upload.
     * 
     * @return void
     */
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection("users")->singleFile();
    }
}
